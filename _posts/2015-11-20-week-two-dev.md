---
title: Week Two of Development (20th Nov)
author: Josh Crowe
---

Our second week of development is coming to an end over this week and we're rapidly becoming a synergetic and highly productive team. 

In celebration of this effectiveness, each member of the official staff team are to be announced: 

Founders

* Crowes 
* Moonsquad (hey! this guy!)

Web Developers

* IRG 
* Crowes 
* Shawishi

Community Managers

* ThisIsDelicious
