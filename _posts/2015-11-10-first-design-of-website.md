---
title: First Design of Website (10th Nov)
---

We've got something to show you. The first design of our homepage is now viewable - obviously, we can't confirm nor deny if this is what you will use at release, but it's some nice inspiration for now. 

[![](http://i.imgur.com/hIYxWlI.jpg?1)](http://imgur.com/hIYxWlI)
