---
title: Work has begun (8th Nov)
author: Josh Crowe
---

Striving to be a unique center of operation for gamers, work needs getting on with. Keeping this in mind, we have begun selecting the team members for our hierarchy. We're also currently choosing our game leaders who are required in development; especially to consider the features they want. 

Do contact Josh Crowe or another admin if you're interested.
